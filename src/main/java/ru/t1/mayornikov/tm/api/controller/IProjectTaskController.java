package ru.t1.mayornikov.tm.api.controller;

import ru.t1.mayornikov.tm.model.Task;

public interface IProjectTaskController {

    void bindTaskToProject();

    void unbindTaskToProject();

}
