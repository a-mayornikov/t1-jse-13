package ru.t1.mayornikov.tm.controller;

import ru.t1.mayornikov.tm.api.controller.IProjectTaskController;
import ru.t1.mayornikov.tm.api.service.IProjectTaskService;
import ru.t1.mayornikov.tm.model.Task;
import ru.t1.mayornikov.tm.util.TerminalUtil;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskToProject() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        final Task task = projectTaskService.bindTaskToProject(projectId, taskId);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[DONE]");
    }

    @Override
    public void unbindTaskToProject() {
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        final Task task = projectTaskService.unbindTaskFromProject(projectId, taskId);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[DONE]");
    }

}