package ru.t1.mayornikov.tm.component;

import ru.t1.mayornikov.tm.api.controller.ICommandController;
import ru.t1.mayornikov.tm.api.controller.IProjectController;
import ru.t1.mayornikov.tm.api.controller.IProjectTaskController;
import ru.t1.mayornikov.tm.api.controller.ITaskController;
import ru.t1.mayornikov.tm.api.repository.ICommandRepository;
import ru.t1.mayornikov.tm.api.repository.IProjectRepository;
import ru.t1.mayornikov.tm.api.repository.ITaskRepository;
import ru.t1.mayornikov.tm.api.service.ICommandService;
import ru.t1.mayornikov.tm.api.service.IProjectService;
import ru.t1.mayornikov.tm.api.service.IProjectTaskService;
import ru.t1.mayornikov.tm.api.service.ITaskService;
import ru.t1.mayornikov.tm.constant.ArgumentConst;
import ru.t1.mayornikov.tm.constant.CommandConst;
import ru.t1.mayornikov.tm.controller.CommandController;
import ru.t1.mayornikov.tm.controller.ProjectController;
import ru.t1.mayornikov.tm.controller.ProjectTaskController;
import ru.t1.mayornikov.tm.controller.TaskController;
import ru.t1.mayornikov.tm.enumerated.Status;
import ru.t1.mayornikov.tm.model.Project;
import ru.t1.mayornikov.tm.repository.CommandRepository;
import ru.t1.mayornikov.tm.repository.ProjectRepository;
import ru.t1.mayornikov.tm.repository.TaskRepository;
import ru.t1.mayornikov.tm.service.CommandService;
import ru.t1.mayornikov.tm.service.ProjectService;
import ru.t1.mayornikov.tm.service.ProjectTaskService;
import ru.t1.mayornikov.tm.service.TaskService;
import ru.t1.mayornikov.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final ITaskController taskController = new TaskController(taskService);

    private void proccessCommands() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println();
            System.out.println("ENTER COMMAND: ");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private void processArguments(final String[] arguments) {
        if (arguments == null || arguments.length < 1) return;
        processArgument(arguments[0]);
        exit();
    }

    private void exit() {
        System.exit(0);
    }

    private void processCommand(final String argument) {
        switch (argument) {
            case ("demostand") : initDemoData(); break;
            case (CommandConst.VERSION) : commandController.showVersion(); break;
            case (CommandConst.ABOUT) : commandController.showAbout(); break;
            case (CommandConst.INFO) : commandController.showSystemInfo(); break;
            case (CommandConst.HELP) : commandController.showHelp(); break;
            case (CommandConst.PROJECT_LIST) : projectController.showProjects(); break;
            case (CommandConst.PROJECT_CREATE) : projectController.createProject(); break;
            case (CommandConst.PROJECT_CLEAR) : projectController.clearProjects(); break;
            case (CommandConst.PROJECT_SHOW_BY_ID) : projectController.showProjectById(); break;
            case (CommandConst.PROJECT_SHOW_BY_INDEX) : projectController.showProjectByIndex(); break;
            case (CommandConst.PROJECT_UPDATE_BY_ID) : projectController.updateProjectById(); break;
            case (CommandConst.PROJECT_UPDATE_BY_INDEX) : projectController.updateProjectByIndex(); break;
            case (CommandConst.PROJECT_REMOVE_BY_ID) : projectController.removeProjectById(); break;
            case (CommandConst.PROJECT_REMOVE_BY_INDEX) : projectController.removeProjectByIndex(); break;
            case (CommandConst.PROJECT_CHANGE_STATUS_BY_ID) : projectController.changeProjectStatusById(); break;
            case (CommandConst.PROJECT_CHANGE_STATUS_BY_INDEX) : projectController.changeProjectStatusByIndex(); break;
            case (CommandConst.PROJECT_START_BY_ID) : projectController.startProjectById(); break;
            case (CommandConst.PROJECT_START_BY_INDEX) : projectController.startProjectByIndex(); break;
            case (CommandConst.PROJECT_COMPLETE_BY_ID) : projectController.completeProjectById(); break;
            case (CommandConst.PROJECT_COMPLETE_BY_INDEX) : projectController.completeProjectByIndex(); break;
            case (CommandConst.TASK_LIST) : taskController.showTasks(); break;
            case (CommandConst.TASK_CREATE) : taskController.createTask(); break;
            case (CommandConst.TASK_CLEAR) : taskController.clearTasks(); break;
            case (CommandConst.TASK_SHOW_BY_ID) : taskController.showTaskById(); break;
            case (CommandConst.TASK_SHOW_BY_INDEX) : taskController.showTaskByIndex(); break;
            case (CommandConst.TASK_SHOW_BY_PROJECT_ID) : taskController.showTaskByProjectId(); break;
            case (CommandConst.TASK_UPDATE_BY_ID) : taskController.updateTaskById(); break;
            case (CommandConst.TASK_UPDATE_BY_INDEX) : taskController.updateTaskByIndex(); break;
            case (CommandConst.TASK_REMOVE_BY_ID) : taskController.removeTaskById(); break;
            case (CommandConst.TASK_REMOVE_BY_INDEX) : taskController.removeTaskByIndex(); break;
            case (CommandConst.TASK_CHANGE_STATUS_BY_ID) : taskController.changeTaskStatusById(); break;
            case (CommandConst.TASK_CHANGE_STATUS_BY_INDEX) : taskController.changeTaskStatusByIndex(); break;
            case (CommandConst.TASK_START_BY_ID) : taskController.startTaskById(); break;
            case (CommandConst.TASK_START_BY_INDEX) : taskController.startTaskByIndex(); break;
            case (CommandConst.TASK_COMPLETE_BY_ID) : taskController.completeTaskById(); break;
            case (CommandConst.TASK_COMPLETE_BY_INDEX) : taskController.completeTaskByIndex(); break;
            case (CommandConst.TASK_BIND_TO_PROJECT) : projectTaskController.bindTaskToProject(); break;
            case (CommandConst.TASK_UNBIND_FROM_PROJECT) : projectTaskController.unbindTaskToProject(); break;
            case (CommandConst.EXIT) : exit(); break;
            default : commandController.showErrorCommand(); break;
        }
    }

    private void initDemoData() {
        projectService.add(new Project("SUPER PROJECT one", Status.IN_PROGRESS));
        projectService.add(new Project("DUPER PROJECT two", Status.NOT_STARTED));
        projectService.add(new Project("donnuiy", Status.COMPLETED));

        taskService.create("delo", "delat");
        taskService.create("BIG DEAL", "GREAT LIFE");
        taskService.create("order", "make model Oleg");
    }

    private void processArgument(final String argument) {
        switch (argument) {
            case (ArgumentConst.VERSION) : commandController.showVersion(); break;
            case (ArgumentConst.ABOUT) : commandController.showAbout(); break;
            case (ArgumentConst.INFO) : commandController.showSystemInfo(); break;
            case (ArgumentConst.HELP) : commandController.showHelp(); break;
            default : commandController.showErrorArgument(); break;
        }
    }

    public void run(final String... args){
        processArguments(args);
        proccessCommands();
    }

}