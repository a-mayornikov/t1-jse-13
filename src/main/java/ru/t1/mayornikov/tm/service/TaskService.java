package ru.t1.mayornikov.tm.service;

import ru.t1.mayornikov.tm.api.repository.ITaskRepository;
import ru.t1.mayornikov.tm.api.service.ITaskService;
import ru.t1.mayornikov.tm.enumerated.Status;
import ru.t1.mayornikov.tm.model.Task;

import java.util.Collections;
import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        else if (description == null || description.isEmpty()) return null;
        return taskRepository.create(name, description);
    }

    @Override
    public Task create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.create(name);
    }

    @Override
    public Task findOne(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findOne(id);
    }

    @Override
    public Task findOne(final Integer index) {
        if (index == null || index < 0) return null;
        return taskRepository.findOne(index);
    }

    @Override
    public Task remove(final Task task) {
        if (task == null) return null;
        return taskRepository.remove(task);
    }

    @Override
    public Task remove(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.remove(id);
    }

    @Override
    public Task remove(final Integer index) {
        if (index == null || index < 0) return null;
        return taskRepository.remove(index);
    }

    @Override
    public Integer getSize() {
        return taskRepository.getSize();
    }

    @Override
    public Task add(final Task task) {
        if(task == null) return null;
        return taskRepository.add(task);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAllByProjectId(String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public Task update(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Task task = findOne(id);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task update(final Integer index, final String name, final String description) {
        if (index == null || index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        final Task task = findOne(index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task changeTaskStatus(final String id, final Status status) {
        if (id == null || id.isEmpty()) return null;
        final Task task = findOne(id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatus(final Integer index, final Status status) {
        if (index == null || index < 0) return null;
        if (index >= taskRepository.getSize()) return null;
        final Task task = findOne(index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public boolean existsById(String id) {
        return findOne(id) != null;
    }

}
